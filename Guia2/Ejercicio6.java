//mostrar numeros primos
package Guia2;



public class Ejercicio6 {
    
    public static void main(String[] args) {
        for (int i = 2; i < 200; i++) {
            if (esPrimo(i)) {
                System.out.println(i);
            }
        }
    }

    public static boolean esPrimo(int n) {
        for (int i = 2; i < n; i++) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }
}