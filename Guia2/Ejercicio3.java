//ingresar un arreglo y ordenar de forma ascendente
package Guia2;
// importamos libreria Arrays arreglo
 import java.util.Arrays;

public class Ejercicio3 {
    public static void main(String[] args) {
        int[] arreglo = {5, 2, 9, 1, 3};
        
        // Ordenar el arreglo en orden ascendente
        Arrays.sort(arreglo);
        
        // Mostrar el arreglo ordenado
        System.out.println("Arreglo ordenado ascendentemente:");
        for (int i = 0; i < arreglo.length; i++) {
            System.out.print(arreglo[i] + " ");
        }
    }

}

