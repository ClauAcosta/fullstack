//ingresar un arreglo y ordenar de forma ascendente
package Guia2;
// importamos Scanner
import java.util.Scanner;


public class Ejercicio4 {
    
public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese un numero: ");
        int numero = sc.nextInt();
        
        // Inicializar el resultado como 1
        int factorial = 1;
        
        // Calcular el factorial
        for (int i = 1; i <= numero; i++) {
            factorial *= i;
        }
        
        // Imprimir el resultado
        System.out.println("El factorial de " + numero + " es " + factorial);
    }
}