
package Guia2;

import java.util.Scanner;



public class Ejercicio7 {
    
    
 public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        // Realizar las tres preguntas
        System.out.println("¿Es herbivoro? (SI/NO)");
        String respuestaHerbivoro = scanner.nextLine().trim().toUpperCase();

        System.out.println("¿Es mamifero? (SI/NO)");
        String respuestaMamifero = scanner.nextLine().trim().toUpperCase();

        System.out.println("¿Es domestico? (SI/NO)");
        String respuestaDomestico = scanner.nextLine().trim().toUpperCase();

        // Determinar el animal seleccionado
        String animalSeleccionado = null;

        if (respuestaHerbivoro.equals("SI") && respuestaMamifero.equals("SI") && respuestaDomestico.equals("NO")) {
            animalSeleccionado = "Alce";
        } else if (respuestaHerbivoro.equals("SI") && respuestaMamifero.equals("SI") && respuestaDomestico.equals("SI")) {
            animalSeleccionado = "Caballo";
        } else if (respuestaHerbivoro.equals("SI") && respuestaMamifero.equals("NO") && respuestaDomestico.equals("NO")) {
            animalSeleccionado = "Caracol";
        } else if (respuestaHerbivoro.equals("NO") && respuestaMamifero.equals("NO") && respuestaDomestico.equals("NO")) {
            animalSeleccionado = "Cóndor";
        } else if (respuestaHerbivoro.equals("NO") && respuestaMamifero.equals("SI") && respuestaDomestico.equals("SI")) {
            animalSeleccionado = "Gato";
        } else if (respuestaHerbivoro.equals("NO") && respuestaMamifero.equals("SI") && respuestaDomestico.equals("NO")) {
            animalSeleccionado = "León";
        } else if (respuestaHerbivoro.equals("NO") && respuestaMamifero.equals("NO") && respuestaDomestico.equals("SI")) {
            animalSeleccionado = "Pitón";
        } else if (respuestaHerbivoro.equals("SI") && respuestaMamifero.equals("NO") && respuestaDomestico.equals("SI")) {
            animalSeleccionado = "Tortuga";
        }

        // Mostrar el resultado
        if (animalSeleccionado != null) {
            System.out.println("El animal seleccionado es: " + animalSeleccionado);
        } else {
            System.out.println("No se pudo determinar el animal seleccionado.");
        }
    }
}
