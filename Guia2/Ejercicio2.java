// Ingresar palabra y verificar si es palindromo, mejorar ya que si coloco 
// espacios en blanco no funciona
package Guia2;
// importamos libreria la clase Scanner
 import java.util.Scanner;
public class Ejercicio2 {
              public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese una palabra: ");
        String palabra = sc.nextLine();
        
        if (esPalindromo(palabra)) {
            System.out.println("La palabra es un palindromo.");
        } else {
            System.out.println("La palabra no es un palindromo.");
            
        }
    }
    
    // funcion esPalindromo
    public static boolean esPalindromo (String palabra) {
           // Convertir la palabra a minúsculas
           palabra = palabra.toLowerCase();
    // La función esPalindromo compara los caracteres
    //de la palabra desde el inicio (i = 0)
    //y el final (j = palabra.length() - 1) al mismo tiempo,
           
        int i = 0;
        int j = palabra.length() - 1;
        
       //avanzando i hacia la derecha
       //y retrocediendo j hacia la izquierda 
       //Si en algún momento los caracteres en estas posiciones no son iguales, 
      //la función devuelve false indicando que no es un palíndromo. 
      //Si se llega al final del bucle sin encontrar ninguna diferencia,
     //la función devuelve true indicando que es un palíndromo
         
       
        while (i < j) {
            if (palabra.charAt(i) != palabra.charAt(j)) {
                return false; // No es un palíndromo
            }
            
            i++;
            j--;
        }
        
        return true; // Es un palíndromo
    }
}

     
          



        