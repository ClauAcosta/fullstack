//fibonacci
package Guia2;
        
import java.util.Scanner;

public class Ejercicio5{
public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ingrese el valor de n: ");
        int n = scanner.nextInt();
        
        int[] fibonacci = new int[n];
        
        // Casos base
        fibonacci[0] = 0;
        if (n > 1) {
            fibonacci[1] = 1;
        }
        
        // Generación de la sucesión de Fibonacci
        for (int i = 2; i < n; i++) {
            fibonacci[i] = fibonacci[i-1] + fibonacci[i-2];
        }
        
        // Mostrar la sucesión de Fibonacci
        System.out.println("Los primeros " + n + " terminos de la sucesión de Fibonacci son:");
        for (int i = 0; i < n; i++) {
            System.out.print(fibonacci[i] + " ");
        }
   }
 }

       
