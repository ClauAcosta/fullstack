
package Guia1;
// importamos libreria la clase Scanner
 import java.util.Scanner;
public class Ejercicio3 {
     public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        // ingresamos primer nro 
        System.out.print("Ingrese el primer numero: ");
        double numero1 = scanner.nextDouble();
        /// ingresamos el segundo
        
        System.out.print("Ingrese el segundo numero: ");
        double numero2 = scanner.nextDouble();
        /// ingresamos las operaciones
        
        double suma = numero1 + numero2;
        double resta = numero1 - numero2;
        double multiplicacion = numero1 * numero2;
        double division = numero1 / numero2;
        // mostramos los resultados
        
        System.out.println("Suma: " + suma);
        System.out.println("Resta: " + resta);
        System.out.println("Multiplicación: " + multiplicacion);
        System.out.println("División: " + division);
        
        scanner.close();
    }
}

   
        