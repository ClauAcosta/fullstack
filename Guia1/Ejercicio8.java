// Ingresar grados celsius
package Guia1;
// importamos libreria la clase Scanner
 import java.util.Scanner;
public class Ejercicio8 {
     public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
                
        System.out.print("Ingresa la temperatura en grados Celsius: ");
        double celsius = scanner.nextDouble();

        double kelvin = 273.15 + celsius;
        double fahrenheit = 1.80* celsius;

        System.out.println("Temperatura en Kelvin: " + kelvin);
        System.out.println("Temperatura en grados Fahrenheit: " + fahrenheit);
    }

       
    }
