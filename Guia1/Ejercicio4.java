// mejorar para que muestre 2 decimales en resultado
package Guia1;
// importamos libreria la clase Scanner
 import java.util.Scanner;
public class Ejercicio4 {
     public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double altura1, altura2, altura3;
        double promedio;

        System.out.println("Ingrese la estatura de la primera persona en metros:");
        altura1 = scanner.nextDouble();

        System.out.println("Ingrese la estatura de la segunda persona en metros:");
        altura2 = scanner.nextDouble();

        System.out.println("Ingrese la estatura de la tercera persona en metros:");
        altura3 = scanner.nextDouble();
        promedio = (altura1 + altura2 + altura3) / 3;
        

        System.out.println("El promedio de altura es: " + promedio + " metros");

        scanner.close();
    }
}
