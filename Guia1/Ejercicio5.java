// Ejercicio 5
package Guia1;
// importamos libreria la clase Scanner
 import java.util.Scanner;
public class Ejercicio5 {
     public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Ingresa el valor del radio de la circunferencia: ");
        double radio = scanner.nextDouble();

        /// como puedo reducir a 2 decimales
        double area = Math.PI * radio * radio;
        double perimetro = 2 * Math.PI * radio;

        System.out.println("El área de la circunferencia es: " + area);
        System.out.println("El perímetro de la circunferencia es: " + perimetro);
    }
}